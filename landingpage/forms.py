from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    status = forms.CharField(widget = forms.TextInput, label ='')
    class Meta:
        model = Status
        fields = {'status'}