$(document).ready(function(){
    $.ajax({
        method: "GET",
        url: "https://www.googleapis.com/books/v1/volumes?q=harry potter",
        success: function(result){
          $("table").empty();
          $("table").append(
            '<thead>' +
              '<tr>' +
                '<th scope="col">Book Picture</th>' +
                '<th scope="col">Book Title</th>' +
                '<th scope="col">Book Author</th>' +
                '<th scope="col">Book Description</th>' +
              '</tr>'+
            '</thead>'
          );
          for(i = 0; i < result.items.length ; i++){
            $("table").append(
              "<tbody>" +
                "<tr>" +
                  "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                  "<td style='width: 30%'>" + result.items[i].volumeInfo.title + "</td>" +
                  "<td style='width: 30%'>" + result.items[i].volumeInfo.authors + "</td>" +
                  "<td>" + result.items[i].volumeInfo.description + "</td>" +
                "</tr>" +
              "</tbody>"
            );
          }
        }
    })

    $("button").click(
      function(){
        var judulBuku = $("#search").val()
        console.log("start", judulBuku)
        $.ajax({
          method: "GET",
          url: "https://www.googleapis.com/books/v1/volumes?q="+judulBuku,
          success: function(result){
            $("table").empty();
            $("table").append(
              '<thead>' +
                '<tr>' +
                  '<th scope="col">Book Picture</th>' +
                  '<th scope="col">Book Title</th>' +
                  '<th scope="col">Book Author</th>' +
                  '<th scope="col">Book Description</th>' +
                '</tr>'+
              '</thead>'
            );
            for(i = 0; i < result.items.length ; i++){
              $("table").append(
                "<tbody>" +
                  "<tr>" +
                    "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                    "<td style='width: 30%'>" + result.items[i].volumeInfo.title + "</td>" +
                    "<td style='width: 30%'>" + result.items[i].volumeInfo.authors + "</td>" +
                    "<td>" + result.items[i].volumeInfo.description + "</td>" +
                  "</tr>" +
                "</tbody>"
              );
            }
          }
        })
      }
    );
  })
  
  $("input").change(
    function(){
      console.log("start start")
    }
  );