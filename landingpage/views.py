from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status

# Create your views here.
def status(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        allstatus = Status.objects.all()
        form = StatusForm()
    
    context = {
        'form': form,
        'allstatus': allstatus,
    }
    return render(request, 'landingpage.html', context)

def profile(request):
    return render(request, 'profile.html')

def books(request):
    return render(request, 'books.html')