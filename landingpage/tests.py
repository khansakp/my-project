from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .forms import *
from .models import *
from selenium import webdriver
import unittest
import time
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class LandingPage_Test(TestCase):
    def test_landingpage_is_exist(self):
      response = Client().get('/')
      self.assertEqual(response.status_code, 200)

    def test_landingpage_404_error(self):
      response = Client().get('/page/')
      self.assertEqual(response.status_code, 404)
    
    def test_using_landingpage(self):
        found = resolve('/')
        self.assertEqual(found.func, status)
    
    def test_uses_landingpage_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landingpage.html')
    
    def test_model_landingpage(self):
        Status.objects.create(status = "Great!")
        hitungJumlah = Status.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    def test_inputhasilForm(self):
        response = self.client.post('/', {
            'status': "Great!",
        })

        hitungJumlah = Status.objects.all().count()
        self.assertEqual(hitungJumlah,1)

    def test_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_profilepage(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)
    
    def test_using_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')
    
    def test_profilepage_has_title(self):
		    response = Client().get('/profile/')
		    html_response = response.content.decode('utf8')
		    self.assertIn("Khansa Khairunisa Putri", html_response)
    
    def test_books_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_books(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)
    
    def test_using_books_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')
    
    def test_books_has_title(self):
		    response = Client().get('/books/')
		    html_response = response.content.decode('utf8')
		    self.assertIn("Cari Buku", html_response)


class FunctionalTest(unittest.TestCase):
    def setUp(self):
      chrome_options = Options()
      chrome_options.add_argument('--dns-prefetch-disable')
      chrome_options.add_argument('--no-sandbox')
      chrome_options.add_argument('--headless')
      chrome_options.add_argument('disable-gpu')
      self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
      super(FunctionalTest, self).setUp()
    
    def tearDown(self):
      self.browser.quit()
    
    def test_LandingPage(self):
      self.browser.get('http://localhost:8000/')
      
      statusTitle = self.browser.find_element_by_tag_name('h1')
      self.assertEquals(statusTitle.text, 'Halo, Apa Kabar?')

      statusbox = self.browser.find_element_by_id('id_status')
      time.sleep(2)
      statusbox.send_keys('Coba Coba')
      time.sleep(2)
      statusbox.submit()
    
    def test_ProfilePage(self):
      self.browser.get('http://localhost:8000/profile')

      title = self.browser.find_element_by_tag_name('h1')
      self.assertEquals(title.text, 'Khansa Khairunisa Putri')
      
      accordion = self.browser.find_element_by_id('aktivitas')
      time.sleep(2)
      accordion.click()
      panel = self.browser.find_element_by_id('isiaktivitas')
      time.sleep(2)
      self.assertEquals(panel.text,'Kuliah di Fakultas Ilmu Komputer Universitas Indonesia')
    
    def test_BookPage(self):
      self.browser.get('http://localhost:8000/books')
      searchbox = self.browser.find_element_by_id('search')
      time.sleep(2)
      searchbox.send_keys('civic')
      time.sleep(2)
      button = self.browser.find_element_by_id('button')
      button.click()





